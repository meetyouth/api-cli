<?php

namespace app\api\controller\v1;

use app\BaseController;

class Index extends BaseController
{

    /**
     * 版本
     *
     * @return void
     */
    public function version(): void
    {
        // tp 版本
        $tp_version = app()->version();
        pass('ok', ['version' => $tp_version]);
    }
}
