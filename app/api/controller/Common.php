<?php

namespace app\api\controller;

use app\BaseController;
use think\response\Json;

class Common extends BaseController
{
    /**
     * miss路由
     *
     * @return Json
     */
    public function miss(): Json
    {
        fail('missing route', null, 404);
    }
}
