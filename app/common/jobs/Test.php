<?php

namespace app\common\jobs;

use think\queue\Job;

class Test
{
    # php think queue:listen --queue test

    /**
     * 测试
     *
     * @param Job $job
     * @param $data
     * @return void
     */
    public function test(Job $job, $data): void
    {
        echo $data['id'] . 'Job 测试';
        $job->delete();
    }

}
