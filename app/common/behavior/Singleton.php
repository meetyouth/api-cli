<?php

namespace app\common\behavior;

trait Singleton
{

    private static $instance;

    /**
     * @param mixed ...$args
     * @return static
     */
    static function getInstance(...$args)
    {
        if (is_null(self::$instance)) {
            self::$instance = new self(...$args);
        }
        return self::$instance;
    }
}