<?php
// +----------------------------------------------------------------------
// | author: 丶长情 <1144318071@qq.com>
// +----------------------------------------------------------------------
// | time：2023/03/15 09:47
// +----------------------------------------------------------------------
namespace app\common\cache;

use app\common\behavior\Singleton;
use app\common\library\Redis;
use RedisException;
use think\Exception;

/**
 * Redis基类
 */
class BaseCache
{
    use Singleton;

    const TIPS = '当前操作太频繁～'; // 操作提示语
    const REQUEST_LIMIT = 'request_limit:%s_%s'; // 频率限制
    const EXPIRE = 3; // 过期时间

    private mixed $redis;

    public function __construct()
    {
        $this->redis = (new Redis())->getRedis();
    }

    /**
     * 调用redis
     * @param $method
     * @param mixed ...$args
     * @return mixed|null
     */
    public function redisPool($method, ...$args): mixed
    {
        $redis = $this->redis;

        $instance = static::getInstance();
        $args = func_get_args();
        unset($args[0]);

        if (method_exists($redis, $method)) {
            return $redis->$method(...$args);
        }
        // 异常
        return $instance->$method($redis, ...$args);
    }

    /**
     * @param string $key 缓存key
     * @param array $field 需要获取的字段
     * @param callable|null $callback 缓存重新创建回调
     * @param int $expire 过期时间
     * @param array $params 需要传到callback的参数
     * @return array|\Redis
     * @throws RedisException
     */
    public function hMGet(string $key, array $field = [], int $expire = 0, callable $callback = null, array $params = []): array|\Redis
    {
        $redis = $this->redis;
        if (!$redis->exists($key)) {
            /**
             * @var array $callbackData
             */
            $callbackData = $this->callback($callback, $params);
            if (empty($callbackData)) {
                return [];
            }
            $redis->hMSet($key, $callbackData);
            $expire > 0 && $redis->expire($key, $expire);
        }
        return !empty($field) ? $redis->hMGet($key, $field) : $redis->hGetAll($key);
    }

    /**
     * @param callable|null $callback
     * @param array $params
     * @return mixed
     */
    private function callback(callable $callback = null, array $params = []): mixed
    {
        return call_user_func($callback, ...$params);
    }

    /**
     * 获取缓存键
     * @return string
     */
    public static function getKey(): string
    {
        $args = func_get_args();
        $key = $args[0];
        unset($args[0]);
        return sprintf($key, ...$args);
    }

    /**
     * 操作限流
     * @param string $method
     * @param int $userId
     * @param int $expire
     * @param string $tips
     * @return void
     * @throws RedisException
     */
    public function requestLimit(string $method, int $userId, int $expire = 3, string $tips = ''): void
    {
        $redis = $this->redis;
        $key = $this->getKey(self::REQUEST_LIMIT, $method, $userId);
        if (!$redis->setnx($key, 1)) {
            fail($tips ?: self::TIPS);
        }
        if ($expire > 0) {
            $redis->expire($key, $expire);
        }
    }

    /**
     * 解锁
     * @param int $userId
     * @param string $method
     * @return void
     * @throws RedisException
     */
    public function freedRequestLimit(string $method, int $userId): void
    {
        $redis = $this->redis;
        $key = $this->getKey(self::REQUEST_LIMIT, $method, $userId);
        $redis->del($key);
    }

    /**
     * 设置过期时间
     * @param string $key
     * @param int $expire
     * @return void
     * @throws RedisException
     */
    public function expire(string $key, int $expire): void
    {
        $this->redis->expire($key, $expire);
    }

    /**
     * 获取redis
     * @return mixed|\Redis|null
     */
    public function getRedis(): mixed
    {
        return $this->redis;
    }

}