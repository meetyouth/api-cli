<?php
// 应用公共文件

use app\common\cache\BaseCache;

if (!function_exists('anti_shake')) {
    /**
     * 防抖动
     * @param int $userId
     * @param int $exp
     * @param string $tips
     * @return void
     */
    function anti_shake(int $userId = 0, int $exp = BaseCache::EXPIRE, string $tips = BaseCache::TIPS): void
    {
        try {
            $method = strtolower(request()->pathinfo());
            BaseCache::getInstance()->requestLimit($method, $userId, $exp, $tips);
        } catch (RedisException $e) {
            fail($e->getMessage());
        }
    }
}

if (!function_exists('pass')) {
    /**
     * 返回成功的API数据到客户端
     * @param string $msg
     * @param null $data
     * @param int $code
     * @param null $type
     * @param array $header
     */
    function pass(string $msg = '', $data = null, int $code = 200, $type = null, array $header = []): void
    {
        oops($msg, $data, $code, $type, $header);
    }
}

if (!function_exists('fail')) {
    /**
     * 返回失败的API数据到客户端
     * @param string $msg
     * @param null $data
     * @param int $code
     * @param null $type
     * @param array $header
     */
    function fail(string $msg = '', $data = null, int $code = 500, $type = null, array $header = []): void
    {
        oops($msg, $data, $code, $type, $header);
    }
}

if (!function_exists('oops')) {
    /**
     * 返回封装后的API数据到客户端
     * @param string $msg
     * @param null $data
     * @param int $code
     * @param null $type
     * @param array $header
     */
    function oops(string $msg = '', $data = null, int $code = 0, $type = null, array $header = [])
    {
        // 毫秒时间戳
        $result = [
            'code' => $code ?: 0,
            'msg' => $msg,
            'time' => microtime(true),
            'data' => $data
        ];

        // 如果未设置类型则自动判断
        $type = $type ?: 'json';

        if (isset($header['statuscode'])) {
            $code = $header['statuscode'];
            unset($header['statuscode']);
        } else {
            //未设置状态码,根据code值判断
            $code = $code >= 1000 || $code < 200 ? 200 : $code;
        }

        $response = \think\Response::create($result, $type, $code)->header($header);
        throw new \think\exception\HttpResponseException($response);
    }
}