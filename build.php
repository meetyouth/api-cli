<?php
// 创建一个空的phar文件
$phar = new Phar('appTools.phar');

// 添加需要打包的文件和目录
$phar->buildFromDirectory(__DIR__);

// 设置入口文件
$phar->setDefaultStub('index.php');

// 将phar文件保存到磁盘
$phar->compressFiles(Phar::GZ);