<?php
// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------

return [
    // 应用地址
    'app_host'         => env('app.host', ''),
    // 应用的命名空间
    'app_namespace'    => '',
    // 是否启用路由
    'with_route'       => true,
    // 默认应用
    'default_app'      => 'api',
    // 默认时区
    'default_timezone' => 'Asia/Shanghai',

    // 应用映射（自动多应用模式有效）
    'app_map'          => [],
    // 域名绑定（自动多应用模式有效）
    'domain_bind'      => [],
    // 禁止URL访问的应用列表（自动多应用模式有效）
    'deny_app_list'    => ['common'],

    // 异常页面的模板文件
    'exception_tmpl'   => app()->getThinkPath() . 'tpl/think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message'    => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'   => false,

    // redis
    'redis'            =>  [
        // 驱动方式
        'type'       => 'redis',
        // 服务器地址
        'host'       => env('redis.host','127.0.0.1'),
        // 端口
        'port'       => env('redis.port', 6379),
        // 密码
        'password'   => env('redis.password'),
        // 缓存select
        'select'     => env('redis.select', 0),
        // persistent是否长连接
        'persistent' => env('redis.persistent', false),
        // 缓存前缀
        'prefix'      => env('redis.prefix', ''),
        // 缓存有效期 0表示永久缓存
        'expire'     => 0
    ],
];
